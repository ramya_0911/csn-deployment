package com.cg.boot.chatservice;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;

 class MessageUpdateTest {

    @Test
    void testGettersAndSetters() {
        // Create an instance of MessageUpdate
        MessageUpdate update = new MessageUpdate();

        // Set a value using the setter
        String content = "Updated Content";
        update.setContent(content);

        // Verify the value using the getter
        assertEquals(content, update.getContent());
    }
}
