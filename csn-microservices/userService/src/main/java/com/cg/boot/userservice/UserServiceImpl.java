package com.cg.boot.userservice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
@Service
public class UserServiceImpl implements UserService {
	
	@Autowired
    private final RestTemplate restTemplate;

    private static final String BASE_URL = "http://user-service/api";
    private static final String BASE_URL1 = "http://placement-service/students";
    private static final String BASE_URL2 = "http://chat-service/chat";
    private static final String BASE_URL3 = "http://staff-service";
    private static final String BASE_URL4 = "http://student-service";
    

        @Autowired
        public UserServiceImpl(RestTemplate restTemplate) {
            this.restTemplate = restTemplate;
        }
        @Override
        public ResponseEntity<?> updateArticle(Long id) {
            // Implement the logic to call the staff-service endpoint for updating an event
        	 return restTemplate.exchange(BASE_URL4 + "/update-article/{id}", HttpMethod.PUT, null,new ParameterizedTypeReference<ResponseEntity<?>>() {}, id);
        }
        @Override
        public ResponseEntity<?> deleteArticle(Long id) {
            // Implement the logic to call the staff-service endpoint for deleting an event
            return restTemplate.exchange(BASE_URL4 + "/student/article/{id}", HttpMethod.DELETE, null, ResponseEntity.class, id);
        }
        @Override
        public ResponseEntity<?> updateEvent(Long id) {
            // Implement the logic to call the staff-service endpoint for updating an event
        	 return restTemplate.exchange(BASE_URL3 + "/update-event/{id}", HttpMethod.PUT, null,new ParameterizedTypeReference<ResponseEntity<?>>() {}, id);
        }

        @Override
        public ResponseEntity<?> deleteEvent(Long id) {
            // Implement the logic to call the staff-service endpoint for deleting an event
            return restTemplate.exchange(BASE_URL3 + "/staff/events/{id}", HttpMethod.DELETE, null, ResponseEntity.class, id);
        }

        @Override
        public ResponseEntity<?> getAllEvents() {
            // Implement the logic to call the staff-service endpoint for getting all events
        	return restTemplate.exchange(BASE_URL3 + "/events", HttpMethod.GET, null,
                    new ParameterizedTypeReference<ResponseEntity<?>>() {});
        }



    @Override
    public ResponseEntity<?> getMessagesForLoggedInUser() {
        return restTemplate.exchange(BASE_URL2 + "/chat/messages", HttpMethod.GET, null, ResponseEntity.class);
    }


    @Override
    public ResponseEntity<?> updateMessage(Long messageId) {
    	return restTemplate.exchange(BASE_URL2 + "/update/{groupId}", HttpMethod.PUT, null,
                new ParameterizedTypeReference<ResponseEntity<?>>() {}, messageId);
    }

    @Override
    public ResponseEntity<?> deleteMessage(Long messageId) {
        return restTemplate.exchange(BASE_URL2 + "/chat/delete/{messageId}", HttpMethod.DELETE, null, ResponseEntity.class, messageId);
    }

    @Override 
    public ResponseEntity<?> updateGroupMessage(String groupId)
    {
    	return restTemplate.exchange(BASE_URL2 + "/update/{groupId}", HttpMethod.PUT, null,
                new ParameterizedTypeReference<ResponseEntity<?>>() {}, groupId);
    }

    @Override
    public ResponseEntity<?> deleteGroupMessage(String groupId) {
        return restTemplate.exchange(BASE_URL2 + "/chat/delete-group/{groupId}", HttpMethod.DELETE, null, ResponseEntity.class, groupId);
    }

    @Override
    public ResponseEntity<?> getGroupMessages(String groupIdentifier) {
        return restTemplate.exchange(BASE_URL2 + "/chat/group-messages?groupIdentifier={groupIdentifier}", HttpMethod.GET, null, ResponseEntity.class, groupIdentifier);
    }

    @Override
    public ResponseEntity<?> updatePost(Long id) {
        return restTemplate.exchange(BASE_URL1 + "/update-post/{id}", HttpMethod.PUT, null,new ParameterizedTypeReference<ResponseEntity<?>>() {}, id);
    }

    @Override
    public ResponseEntity<?> deletePost(Long id) {
        return restTemplate.exchange(BASE_URL1 + "/delete-post/{id}", HttpMethod.DELETE, null,new ParameterizedTypeReference<ResponseEntity<?>>() {}, id);
    }

    @Override
    public ResponseEntity<?> getAllPosts() {
        return restTemplate.exchange(BASE_URL1 + "/posts", HttpMethod.GET, null,
                new ParameterizedTypeReference<ResponseEntity<?>>() {});
    }

    @Override
    public ResponseEntity<?> getStudents() {
        return restTemplate.exchange(BASE_URL1 + "/students", HttpMethod.GET, null,
                new ParameterizedTypeReference<ResponseEntity<?>>() {});
    }

    @Override
    public ResponseEntity<?> getUsers() {
        return restTemplate.exchange(BASE_URL + "/registrations", HttpMethod.GET, null,
                new ParameterizedTypeReference<ResponseEntity<?>>() {});
    }

    @Override
    public ResponseEntity<?> getNewsFeed() {
        return restTemplate.exchange(BASE_URL + "/newsfeed", HttpMethod.GET, null,
                new ParameterizedTypeReference<ResponseEntity<?>>() {});
    }

    @Override
    public ResponseEntity<?> approveUser(Long id) {
        return restTemplate.postForEntity(BASE_URL + "/users/{id}/approve", null, ResponseEntity.class, id);
    }

    @Override
    public ResponseEntity<?> approveNewsFeed(Long id) {
        return restTemplate.postForEntity(BASE_URL + "/news-feed/approve/{id}", null, ResponseEntity.class, id);
    }


    
}