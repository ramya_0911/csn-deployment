package com.cg.boot.userservice;
import javax.servlet.http.HttpSession;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.cg.boot.SpringDataMain;

import static org.mockito.Mockito.*;
@SpringBootTest(classes =SpringDataMain.class)
class LoginControllerTest {

    @Mock
    private ApprovedRegistrationRepository approvedRegistrationRepository;

    @Mock
    private HttpSession session;

    @InjectMocks
    private LoginController loginController;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testLoginUserWithValidCredentials() {
        LoginController.LoginCredentials loginCredentials = new LoginController.LoginCredentials("username", "password", "role");
        ApprovedRegistration user = new ApprovedRegistration();
        when(approvedRegistrationRepository.findByUsernameAndPasswordAndRole("username", "password", "role"))
                .thenReturn(user);

        ResponseEntity<String> response = loginController.loginUser(loginCredentials, session);

        verify(session).setAttribute("loggedInUser", user);
        Assertions.assertEquals(HttpStatus.OK, response.getStatusCode());
        Assertions.assertEquals("Login successful!", response.getBody());
    }

    @Test
    void testLoginUserWithInvalidCredentials() {
        LoginController.LoginCredentials loginCredentials = new LoginController.LoginCredentials("username", "password", "role");
        when(approvedRegistrationRepository.findByUsernameAndPasswordAndRole("username", "password", "role"))
                .thenReturn(null);

        ResponseEntity<String> response = loginController.loginUser(loginCredentials, session);

        verify(session, never()).setAttribute(anyString(), any());
        Assertions.assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        Assertions.assertEquals("Invalid login credentials.", response.getBody());
    }
    @Test
    void testLoginCredentialsDefaultConstructor() {
        // Arrange & Act
        LoginController.LoginCredentials loginCredentials = new LoginController.LoginCredentials();

        // Assert
        Assertions.assertNotNull(loginCredentials);
    }

    @Test
    void testLoginCredentialsParameterizedConstructorAndGettersSetters() {
        // Arrange
        String username = "testuser";
        String password = "testpassword";
        String role = "testrole";

        // Act
        LoginController.LoginCredentials loginCredentials = new LoginController.LoginCredentials(username, password, role);

        // Assert
        Assertions.assertEquals(username, loginCredentials.getUsername());
        Assertions.assertEquals(password, loginCredentials.getPassword());
        Assertions.assertEquals(role, loginCredentials.getRole());
    }

    @Test
    void testLoginCredentialsSetters() {
        // Arrange
        LoginController.LoginCredentials loginCredentials = new LoginController.LoginCredentials();

        // Act
        String username = "testuser";
        String password = "testpassword";
        String role = "testrole";

        loginCredentials.setUsername(username);
        loginCredentials.setPassword(password);
        loginCredentials.setRole(role);

        // Assert
        Assertions.assertEquals(username, loginCredentials.getUsername());
        Assertions.assertEquals(password, loginCredentials.getPassword());
        Assertions.assertEquals(role, loginCredentials.getRole());
    }

}
