package com.cg.boot.userservice;
import static org.mockito.Mockito.verify;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.fasterxml.jackson.databind.ObjectMapper;

import com.cg.boot.SpringDataMain;
@SpringBootTest(classes =SpringDataMain.class)

@AutoConfigureMockMvc
class RegistrationControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private RegisteredTryRepository registeredTryRepository;

    @BeforeEach
     void setup() {
        // Setup any necessary mock behavior for the registeredTryRepository
    }
    @Test
    void testRegisterUser() throws Exception {
        // Create a sample RegisteredTry object
        RegisteredTry registeredTry = new RegisteredTry();
        registeredTry.setId(1L);
        registeredTry.setFirstName("Neethu");
        registeredTry.setLastName("P");
        registeredTry.setAge(22);
        registeredTry.setUsername("neethup");
        registeredTry.setPassword("Neethu123");
        registeredTry.setRole("student");
        // ... Set other properties

        // Perform the POST request to the /register endpoint
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.post("/users/register")
                .content(asJsonString(registeredTry))
                .contentType(MediaType.APPLICATION_JSON))
                .andReturn();

        // Print the response status
        System.out.println("Response status: " + result.getResponse().getStatus());

        // Verify that the save method was called on the registeredTryRepository
       // verify(registeredTryRepository).save(registeredTry);
    }

    // Helper method to convert an object to JSON string
    private String asJsonString(Object obj) {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            return objectMapper.writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
