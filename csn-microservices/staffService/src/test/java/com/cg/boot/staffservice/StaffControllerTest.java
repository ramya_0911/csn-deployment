package com.cg.boot.staffservice;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpSession;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.ResponseEntity;


@WebMvcTest(StaffController.class)
class StaffControllerTest {

    @MockBean
    private StudentRepository studentRepository;

    @MockBean
    private EventRepository eventRepository;

    @Mock
    private HttpSession session;

    @InjectMocks
    private StaffController staffController;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testGetStudents() {
        // Mocking the session attribute
        ApprovedRegistration user = new ApprovedRegistration();
        when(session.getAttribute(eq("loggedInUser"))).thenReturn(user);

        // Mocking the student repository
        List<Student> students = Arrays.asList(new Student(), new Student());
        when(studentRepository.findAll()).thenReturn(students);

        // Calling the controller method
        ResponseEntity<?> response = staffController.getStudents(session);

        // Verifying the response
        assertThat(response.getBody()).isEqualTo(students);
    }

    @Test
    void testAddEvent() {
        // Mocking the session attribute
        ApprovedRegistration user = new ApprovedRegistration();
        when(session.getAttribute(eq("loggedInUser"))).thenReturn(user);

        // Mocking the event request
        StaffController.EventRequest eventRequest = new StaffController.EventRequest();
        eventRequest.setEvent("Test Event");
        eventRequest.setDateOfEvent(LocalDate.now());
        eventRequest.setPeriod(1);

        // Calling the controller method
        ResponseEntity<?> response = staffController.addEvent(eventRequest, session);

        // Verifying the event repository save method is called
        verify(eventRepository).save(any(Event.class));

        // Verifying the response
        assertThat(response.getStatusCodeValue()).isEqualTo(200);
        assertThat(response.getBody()).isInstanceOf(Event.class);
        Event savedEvent = (Event) response.getBody();
        assertThat(savedEvent.getEvent()).isEqualTo("Test Event");
    }

    @Test
    void testUpdateEvent() {
        // Mocking the session attribute
        ApprovedRegistration user = new ApprovedRegistration();
        when(session.getAttribute(eq("loggedInUser"))).thenReturn(user);
        Event event = null;

        // Mocking the response entity for not found case
        ResponseEntity<?> notFoundResponse = ResponseEntity.notFound().build();
        // Mocking an existing event
        Event existingEvent = new Event();
        existingEvent.setId(1L);
        existingEvent.setEvent("Existing Event");
        existingEvent.setDateOfEvent(LocalDate.now());
        existingEvent.setPeriod(2);
        when(eventRepository.findById(eq(1L))).thenReturn(Optional.of(existingEvent));

        // Mocking the event request
        StaffController.EventRequest eventRequest = new StaffController.EventRequest();
        eventRequest.setEvent("Updated Event");
        eventRequest.setDateOfEvent(LocalDate.now());
        eventRequest.setPeriod(3);

        // Calling the controller method
        ResponseEntity<?> response = staffController.updateEvent(1L, eventRequest, session);

        // Verifying the event repository save method is called
        verify(eventRepository).save(any(Event.class));

        // Verifying the response
        assertThat(response.getStatusCodeValue()).isEqualTo(200);
        assertThat(response.getBody()).isInstanceOf(Event.class);
        Event updatedEvent = (Event) response.getBody();
        assertThat(updatedEvent.getEvent()).isEqualTo("Updated Event");
    }
    @Test
    void testUpdateEvent_NotFound() {
        // Mocking the session attribute
        ApprovedRegistration user = new ApprovedRegistration();
        when(session.getAttribute(eq("loggedInUser"))).thenReturn(user);
        Event event = null;

        // Mocking the response entity for not found case
        ResponseEntity<?> notFoundResponse = ResponseEntity.notFound().build();

        // Mocking the event request
        StaffController.EventRequest eventRequest = new StaffController.EventRequest();
        eventRequest.setEvent("Updated Event");
        eventRequest.setDateOfEvent(LocalDate.now());
        eventRequest.setPeriod(3);

        // Mocking the optional event as empty
        Optional<Event> optionalEvent = Optional.empty();
        when(eventRepository.findById(eq(1L))).thenReturn(optionalEvent);

        // Calling the controller method
        ResponseEntity<?> response = staffController.updateEvent(1L, eventRequest, session);

        // Verifying that the event repository save method is not called
        verify(eventRepository, never()).save(any(Event.class));

        // Verifying the response for not found case
        assertThat(response).isEqualTo(notFoundResponse);
    }


    @Test
    void testDeleteEvent() {
        // Mocking the session attribute
        ApprovedRegistration user = new ApprovedRegistration();
        when(session.getAttribute(eq("loggedInUser"))).thenReturn(user);

        // Mocking the response entity for not found case
        ResponseEntity<?> notFoundResponse = ResponseEntity.notFound().build();

        // Mocking an existing event
        Event existingEvent = new Event();
        existingEvent.setId(1L);
        existingEvent.setEvent("Existing Event");
        existingEvent.setDateOfEvent(LocalDate.now());
        existingEvent.setPeriod(2);
        when(eventRepository.findById(eq(1L))).thenReturn(Optional.of(existingEvent));

        // Calling the controller method with a non-null event
        ResponseEntity<Event> responseNonNullEvent = staffController.deleteEvent(1L, session);

        // Verifying the event repository delete method is called
        verify(eventRepository).delete(eq(existingEvent));

        // Verifying the response
        assertThat(responseNonNullEvent.getStatusCodeValue()).isEqualTo(200);
        assertThat(responseNonNullEvent.getBody()).isInstanceOf(Event.class);
        Event deletedEvent = responseNonNullEvent.getBody();
        assertThat(deletedEvent.getEvent()).isEqualTo("Existing Event");

        // Resetting the mocks
        reset(eventRepository);

        // Calling the controller method with a null event
        ResponseEntity<Event> responseNullEvent = staffController.deleteEvent(2L, session);

        // Verifying the event is null and returning not found response
        assertThat(responseNullEvent).isEqualTo(notFoundResponse);
        verify(eventRepository, never()).delete(any(Event.class));
    }



    @Test
    void testGetAllEvents() {
        // Mocking the event repository
        List<Event> events = Arrays.asList(new Event(), new Event());
        when(eventRepository.findAll()).thenReturn(events);

        // Calling the controller method
        ResponseEntity<List<Event>> response = staffController.getAllEvents();

        // Verifying the response
        assertThat(response.getBody()).isEqualTo(events);
    }
}
