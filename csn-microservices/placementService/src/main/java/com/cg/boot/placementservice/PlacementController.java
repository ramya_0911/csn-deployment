package com.cg.boot.placementservice;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpSession;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping("/students")
public class PlacementController {

    @Autowired
    private StudentRepository studentRepository;
    
    @Autowired
    private PostsRepository postsRepository;
    
    @Autowired
    private RestTemplate restTemplate;
    public PlacementController(PostsRepository postsRepository) {
		this.postsRepository=postsRepository;
	}
	public void setRestTemplate(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }
    @PostMapping
    public ResponseEntity<String> addStudent(@RequestBody StudentDetails studentDetails, HttpSession session) {
            Student student = new Student();
            student.setId(studentDetails.getId());
            student.setName(studentDetails.getName());
            student.setDepartment(studentDetails.getDepartment());
            student.setBacklogs(studentDetails.getBacklogs());
            student.setPercentage(studentDetails.getPercentage());
            studentRepository.save(student);
            return ResponseEntity.ok("Student added successfully!");
    }
    public final String getUsernameFromUserService() {
    	
        String userServiceUrl = "http://user-service/api";
        
        ResponseEntity<String> response = restTemplate.exchange(
        		
            userServiceUrl + "/username",
            
            HttpMethod.GET,
            
            null,
            
            String.class
        );
        String username = response.getBody();
        return username;
    }

    @PostMapping("/add-post")
    public ResponseEntity<Posts> addPost(@RequestBody PostDetails postDetails, HttpSession session) {
        ApprovedRegistration user = (ApprovedRegistration) session.getAttribute("loggedInUser");
        
        if(user!=null) {
        	
        	Posts post = new Posts();
        	
            post.setId(postDetails.getId());
            
            post.setTitle(postDetails.getTitle());
            
            post.setContent(postDetails.getContent());
            
            post.setUsername(getUsernameFromUserService());
            
            post.setPostedDate(new Date());
            
            postsRepository.save(post);
            
            return ResponseEntity.ok(post);
        }
        else {
        	Posts post = new Posts();
            post.setId(postDetails.getId());
            post.setTitle(postDetails.getTitle());
            post.setContent(postDetails.getContent());
            post.setUsername("anu");
            post.setPostedDate(new Date());
            postsRepository.save(post);
            return ResponseEntity.ok(post);
        }
        }
    @SuppressWarnings("unused")
	@PutMapping("/update-post/{id}")
    public ResponseEntity<Posts> updatePost(@PathVariable("id") Long id, 
            @RequestBody PostDetails postDetails, HttpSession session) {
        ApprovedRegistration user = (ApprovedRegistration) session.getAttribute("loggedInUser");
        Posts post = postsRepository.findById(id).orElse(null);

        if (post == null) {
            return ResponseEntity.notFound().build();
        }

        post.setTitle(postDetails.getTitle());
        post.setContent(postDetails.getContent());
        postsRepository.save(post);
        return ResponseEntity.ok(post);
    }
    @SuppressWarnings("unused")
	@DeleteMapping("/delete-post/{id}")
    public ResponseEntity<Posts> deletePost(@PathVariable("id") Long id, HttpSession session) {
        ApprovedRegistration user = (ApprovedRegistration) session.getAttribute("loggedInUser");
        Posts post = postsRepository.findById(id).orElse(null);

        if (post == null) {
            return ResponseEntity.notFound().build();
        }


        postsRepository.delete(post);
        return ResponseEntity.ok(post);
    }
    @GetMapping("/posts")
    public ResponseEntity<List<Posts>> getAllPosts() {
        List<Posts> postsList = postsRepository.findAll();
        return ResponseEntity.ok(postsList);
    }

    @SuppressWarnings("unused")
	@GetMapping("/students")
    public ResponseEntity<?> getStudents(HttpSession session) {
        ApprovedRegistration user = (ApprovedRegistration) session.getAttribute("loggedInUser");
        
        List<Student> students = studentRepository.findAll();
        
        return ResponseEntity.ok(students);
    }

    static class PostDetails {
    	@NotBlank
        private Long id;
    	@NotBlank @Size(min=3,max=10)
        private String title;
    	@NotBlank @Size(min=10,max=50)
        private String content;

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }
    }

    static class StudentDetails {
        private long id;
        private String name;
        private String department;
        private int backlogs;
        private double percentage;

        public long getId() {
            return id;
        }

        public void setId(Long long1) {
            this.id = long1;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDepartment() {
            return department;
        }

        public void setDepartment(String department) {
            this.department = department;
        }

        public int getBacklogs() {
            return backlogs;
        }

        public void setBacklogs(int backlogs) {
            this.backlogs = backlogs;
        }

        public double getPercentage() {
            return percentage;
        }

        public void setPercentage(double percentage) {
            this.percentage = percentage;
        }
    }

	public void setPostsRepository(PostsRepository postsRepository) {
		this.postsRepository=postsRepository;
		
	}
	public void setStudentRepository(StudentRepository studentRepository) {
	this.studentRepository=studentRepository;
		
	}
}
