package com.cg.boot.placementservice;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;

public class ApprovedRegistrationTest {

    @Test
    public void testGettersAndSetters() {
        // Create an instance of ApprovedRegistration
        ApprovedRegistration registration = new ApprovedRegistration();

        // Set values using setters
        registration.setId(1L);
        registration.setFirstName("John");
        registration.setLastName("Doe");
        registration.setAge(25);
        registration.setUsername("johndoe");
        registration.setPassword("password");
        registration.setRole("admin");

        // Verify values using getters
        assertEquals(1L, registration.getId());
        assertEquals("John", registration.getFirstName());
        assertEquals("Doe", registration.getLastName());
        assertEquals(25, registration.getAge());
        assertEquals("johndoe", registration.getUsername());
        assertEquals("password", registration.getPassword());
        assertEquals("admin", registration.getRole());
    }

//    @Test
//    public void testConstructor() {
//        // Create an instance of ApprovedRegistration using constructor
//        ApprovedRegistration registration = new ApprovedRegistration();
//
//        // Verify values using getters
//        assertEquals(1L, registration.getId());
//        assertEquals("John", registration.getFirstName());
//        assertEquals("Doe", registration.getLastName());
//        assertEquals(25, registration.getAge());
//        assertEquals("johndoe", registration.getUsername());
//        assertEquals("password", registration.getPassword());
//        assertEquals("admin", registration.getRole());
//    }

    @Test
    public void testDefaultConstructor() {
        // Create an instance of ApprovedRegistration using default constructor
        ApprovedRegistration registration = new ApprovedRegistration();

        // Verify that the instance is not null
        assertNotNull(registration);
    }
}
