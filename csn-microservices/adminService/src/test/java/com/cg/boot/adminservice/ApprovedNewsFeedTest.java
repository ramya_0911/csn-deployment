package com.cg.boot.adminservice;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;
import java.util.Date;

 class ApprovedNewsFeedTest {

    @Test
    void testGettersAndSetters() {
        // Create an instance of ApprovedNewsFeed
        ApprovedNewsFeed newsFeed = new ApprovedNewsFeed();

        // Set values using setters
        Long id = 1L;
        String title = "Test Title";
        String content = "Test Content";
        Date postedDate = new Date();
        Long studentId = 123L;
        String postedBy = "John Doe";

        newsFeed.setId(id);
        newsFeed.setTitle(title);
        newsFeed.setContent(content);
        newsFeed.setPostedDate(postedDate);
        newsFeed.setStudentId(studentId);
        newsFeed.setPostedBy(postedBy);

        // Verify the values using getters
        assertEquals(id, newsFeed.getId());
        assertEquals(title, newsFeed.getTitle());
        assertEquals(content, newsFeed.getContent());
        assertEquals(postedDate, newsFeed.getPostedDate());
        assertEquals(studentId, newsFeed.getStudentId());
        assertEquals(postedBy, newsFeed.getPostedBy());
    }
}
