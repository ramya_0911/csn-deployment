package com.cg.boot.studentservice;

import java.util.Date;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/student")
public class StudentController {

    @Autowired
    private NewsFeedRepository newsFeedRepository;
    @Autowired
    private ApprovedNewsFeedRepository approvedNewsFeedRepository;

    @PostMapping("/addArticle")
    public ResponseEntity<String> addArticle(
            @RequestParam("title") String title,
            @RequestParam("content") String content,
            HttpSession session) {
        NewsFeed newsFeed = new NewsFeed();
        newsFeed.setTitle(title);
        newsFeed.setContent(content);
        newsFeed.setPostedDate(new Date());
        newsFeedRepository.save(newsFeed);
        return ResponseEntity.ok("Article added successfully.");
    }

    @PutMapping("/updateArticle/{id}")
    public ResponseEntity<String> updateArticle(
            @PathVariable("id") Long id,
            @RequestParam("title") String title,
            @RequestParam("content") String content,
            HttpSession session) {
        ApprovedNewsFeed newsFeed = approvedNewsFeedRepository.findById(id).orElse(null);

        if (newsFeed == null) {
            return ResponseEntity.notFound().build();
        }

        newsFeed.setTitle(title);
        newsFeed.setContent(content);
        newsFeed.setPostedDate(new Date());

        approvedNewsFeedRepository.save(newsFeed);

        return ResponseEntity.ok("Article updated successfully.");
    }

    @DeleteMapping("/deleteArticle/{id}")
    public ResponseEntity<String> deleteArticle(@PathVariable("id") Long id, HttpSession session) {
        ApprovedNewsFeed newsFeed = approvedNewsFeedRepository.findById(id).orElse(null);

        if (newsFeed == null) {
            return ResponseEntity.notFound().build();
        }

        approvedNewsFeedRepository.delete(newsFeed);

        return ResponseEntity.ok("Article deleted successfully.");
    }

}
