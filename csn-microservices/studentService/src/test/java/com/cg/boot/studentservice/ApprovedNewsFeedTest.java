package com.cg.boot.studentservice;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import java.util.Date;

 class ApprovedNewsFeedTest {

    @Test
   void testApprovedNewsFeed() {
        Long id = 1L;
        String title = "Test Title";
        String content = "Test Content";
        Date postedDate = new Date();
        Long studentId = 123L;
        String postedBy = "Test User";

        ApprovedNewsFeed approvedNewsFeed = new ApprovedNewsFeed(id, title, content, postedDate, studentId, postedBy);

        Assertions.assertEquals(id, approvedNewsFeed.getId());
        Assertions.assertEquals(title, approvedNewsFeed.getTitle());
        Assertions.assertEquals(content, approvedNewsFeed.getContent());
        Assertions.assertEquals(postedDate, approvedNewsFeed.getPostedDate());
        Assertions.assertEquals(studentId, approvedNewsFeed.getStudentId());
        Assertions.assertEquals(postedBy, approvedNewsFeed.getPostedBy());
    

}
    @Test
    public void testAllArgsConstructor() {
        Long id = 1L;
        String title = "Test Title";
        String content = "Test Content";
        Date postedDate = new Date();
        Long studentId = 123L;
        String postedBy = "Test User";

        ApprovedNewsFeed newsFeed = new ApprovedNewsFeed(id, title, content, postedDate, studentId, postedBy);

        Assertions.assertEquals(id, newsFeed.getId());
        Assertions.assertEquals(title, newsFeed.getTitle());
        Assertions.assertEquals(content, newsFeed.getContent());
        Assertions.assertEquals(postedDate, newsFeed.getPostedDate());
        Assertions.assertEquals(studentId, newsFeed.getStudentId());
        Assertions.assertEquals(postedBy, newsFeed.getPostedBy());
    }
}
 

